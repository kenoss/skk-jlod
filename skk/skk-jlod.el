;;; skk-jlod.el --- $B3HD%%m!<%^;zF~NO(B "JLOD" $B$r(B SKK $B$G;H$&$?$a$N@_Dj(B -*- coding: iso-2022-jp -*-

;; Copyright (C) 2003, 2007 IRIE Tetsuya <irie@t.email.ne.jp>
;; Copyright (C) 2012 KEN Okada <tokada@kurims.kyoto-u.ac.jp>

;; Author: IRIE Tetsuya <irie@t.email.ne.jp>
;; Author: KEN Okada <tokada@kurims.kyoto-u.ac.jp>
;; Keywords: japanese, mule, input method

;; This file is part of Daredevil SKK.

;; Daredevil SKK is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or
;; (at your option) any later version.

;; Daredevil SKK is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Daredevil SKK, see the file COPYING.  If not, write to
;; the Free Software Foundation Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; $B$3$N%W%m%0%i%`$O(B skk-act.el $B$r85$K$7$F:n$i$l$F$$$^$9(B.
;;
;; dvorak $BG[Ns$G$N3HD%%m!<%^;zF~NO(B "JLOD" $B$r(B SKK $B$G;H$&$?$a$N@_Dj$G$9(B.
;; "JLOD" $B$K$D$$$F$O!$0J2<$N(B URL $B$r;2>H$7$F2<$5$$(B.
;;   http://www.mikage.to/jlod/
;;
;; $B;H$$J}(B - $B2<5-$N@_Dj$r(B .skk $B$K2C$($F$/$@$5$$(B.
;;          $B$=$N8e(B Emacs(Mule) $B$r:F5/F0$9$l$P(B skk $B$K$h$k(B JLOD $B$G$N(B
;;          $BF~NO$,2DG=$G$9(B.
;;
;;          (setq skk-use-jlod t)
;;
;;
;;   $BCm0U(B 1 - JLOD $B$G$O(B "q" $B$r(B "$B$*$s(B" $B$NF~NO$K;H$&$N$G!$(B"q" $B$N$b$H$b$H$N(B
;;            $B5!G=$G$"$k(B `skk-toggle-kana' $B$O(B "\" $B$K3dEv$F$F$$$^$9(B.
;;            SKK $BI8=`$G(B "\" $B$N(B `skk-input-by-code-or-menu' $B$O3dEv$F$F(B
;;            $B$$$J$$$N$G%^%K%e%"%k$G8F=P$9I,MW$,$"$j$^$9(B.
;;
;;        2 - $BF1MM$K(B "Q" $B$b;HMQ$G$-$^$;$s$N$G(B
;;            `skk-set-henkan-point-subr' $B$O(B "|" $B$K3dEv$F$F$$$^$9(B.
;;
;;        3 - $B=c@5$N(B ACT $B$G$O(B "la" $B$G(B "$B$!(B" $B$rF~NO$7$^$9(B. $B$7$+$7(B
;;            SKK $B$G$O(B l $B$r(B ASCII/$B$+$J%b!<%I$N@Z$jBX$(%-!<$H$7$F(B
;;            $B;HMQ$9$k$N$G!$(B"`a" $B$G(B "$B$!(B" $B$rF~NO$G$-$k$h$&$K$7$F$$$^$9(B.
;;            "la" $B$G(B "$B$!(B" $B$HF~NO$G$-$k$h$&$K$9$k$?$a$K$O(B,
;;            ~/.skk.el $B$K(B
;;
;;            (setq skk-jlod-use-skk-compatible-jlod nil)
;;
;;            $B$rDI2C$7$F$/$@$5$$(B. $BC"$7(B, "l" $B$N3d$jEv$F$OJQ99$7$F$$$^$;$s(B.
;;
;;        4 - SKK $BI8=`$N(B "z*" ($B!V!A!W!V!D!W$J$I(B)$B!$(B"x*" ($B!V$#!W!V$n!W$J(B
;;            $B$I(B)$B$O(B "`*" $B$K3dEv$F$F$$$^$9(B.
;;
;;        5 - $B%G%U%)%k%H$G$O;R2;$N8e$N(B "y" $B$K$O(B2$B=EJl2;$N(B "ui" $B$r3dEv$F(B
;;            $B$F$$$k$N$G!$(B"y" $B$r;H$C$?Y92;$NF~NO$OL58z$G$9(B.
;;
;;   $B%-!<3dEv$FJQ99E@(B
;;                                  SKK$BI8=`(B  SKK$B8_49(BJLOD   JLOD
;;	`skk-toggle-kana'              q         \          \
;;	`skk-set-henkan-point-subr'    Q         |          |
;;	`skk-input-by-code-or-menu'    \     $B3dEv$F$J$7(B  $B3dEv$F$J$7(B
;;	`skk-purge-from-jisyo'         X     $B3dEv$F$J$7(B  $B3dEv$F$J$7(B
;;	$B>.Jl2;(B "$B$!(B"                    xa        `a         la

;;; Code:

(eval-when-compile
  (require 'skk-macs)
  (require 'skk-vars))

(require 'cl)

;; srfi-1:filter, srfi-1:zip
(defun srfi-1:filter (pred xs)
  (loop for x in xs
        when (funcall pred x)
        collect x))
(defun srfi-1:zip (&rest xss)
  (apply #'mapcar* #'list xss))


(defun append-map (proc &rest lists)
  (apply #'append (apply #'mapcar* (cons proc lists))))

(defvar skk-jlod-aoeui-list
  '("a" "o" "e" "u" "i" ";" "q" "j" "k" "x" "'" "," "." "p" "y"))

(defun skk-jlod-aiueo-words-to-aoeui-list (words)
  (mapcar (lambda (n)
	    (nth (- n 1) (mapcar #'string (append words nil))))
	  '(1 5 4 3 2)))

(defun skk-jlod-make-rules (in-char char-sufix-list out-hiragana hiragana-sufix-list)
  ;; char-sufix-list, hiragana-sufix-list $B$O(Batom$B$G$b$h$$(B
  (defun aux (lis)
    (let ((size (apply #'max (cons 1 (mapcar #'length (srfi-1:filter #'listp lis))))))
      (mapcar (lambda (x) (if (atom x)
			      (make-list size x)
			    x))
	      lis)))
  (mapcar (lambda (x)
	    (let ((input-alphabet (concat (car x) (cadr x)))
		  (output-hiragana (concat (caddr x) (cadddr x))))
	      (list input-alphabet nil
		    (cons (japanese-katakana output-hiragana)
			  output-hiragana))))
	  (apply #'srfi-1:zip
		 (aux (list in-char char-sufix-list
			    out-hiragana hiragana-sufix-list)))))

(defvar skk-jlod-special-key-list
  '(("wo" nil ("$B%r(B" . "$B$r(B"))
    ("nn" nil ("$B%s(B" . "$B$s(B"))
    ("'" nil ("$B%C(B" . "$B$C(B"))
    ;; $BFC<l%-!<$H%(%9%1!<%W(B
    ("\\" nil skk-toggle-kana)
    ("|" nil skk-set-henkan-point-subr)
    ("`|" nil "|")
    ("`'" nil "'")
    ("`;" nil ";")
    ("`:" nil ":")
    ("`\"" nil "\"")
    ;; $BI8=`$N(B z* $B$NCV$-49$((B
    ("`," nil "$B!E(B")
    ("`-" nil "$B!A(B")
    ("`." nil "$B!D(B")
    ("`/" nil "$B!&(B")
    ("`[" nil "$B!X(B")
    ("`]" nil "$B!Y(B")
    ("`d" nil "$B"+(B")
    ("`h" nil "$B"-(B")
    ("`t" nil "$B",(B")
    ("`n" nil "$B"*(B")))

(defvar skk-jlod-small-char-list
  (let ((prefix (if skk-jlod-use-skk-compatible-jlod "`" "l")))
    (append-map (lambda (lis)
		  (skk-jlod-make-rules (concat prefix (car lis)) "" (cadr lis) ""))
		'(("a" "$B$!(B")
		  ("i" "$B$#(B")
		  ("u" "$B$%(B")
		  ("e" "$B$'(B")
		  ("o" "$B$)(B")
		  ("ca" "$B%u(B")
		  ("ce" "$B%v(B")
		  ("wa" "$B$n(B")
		  ("we" "$B$q(B")
		  ("wi" "$B$p(B")
		  ("va" "$B$c(B")
		  ("vo" "$B$g(B")
		  ("vu" "$B$e(B")))))

;; $BFs=EJl2;3HD%4^$`4pK\E*$JJl2;(B, $B@62;(B, $BBy2;(B
(defvar skk-jlod-basic-list
  (append
   (skk-jlod-make-rules
    "" (reverse (nthcdr 5 (reverse skk-jlod-aoeui-list)))
    "" (let ((word-list (skk-jlod-aiueo-words-to-aoeui-list "$B$"$$$&$($*(B")))
	 (append word-list (mapcar (lambda (x) (concat x "$B$s(B")) word-list))))
   '(("wy" nil ("$B$&$$(B" . "$B$&$$(B")))
   (srfi-1:filter
    (lambda (x) (not (or (equal "wo" (car x))
			 (equal "wy" (car x)))))
    (append-map
     (lambda (lis) (apply #'skk-jlod-make-rules lis))
     (mapcar (lambda (lis)
	       (let ((word-list (cadr lis)))
		 (list (car lis) skk-jlod-aoeui-list
		       "" (append word-list
				  (mapcar (lambda (x) (concat x "$B$s(B"))
					  word-list)
				  (mapcar* (lambda (x y) (concat x y))
					   word-list
					   '("$B$$(B" "$B$&(B" "$B$$(B" "$B$&(B" "$B$$(B"))))))
	     (cons '("w" ("$B$o(B" "$B$&$)(B" "$B$&$'(B" "$B$&(B" "$B$&$#(B"))
		   (mapcar (lambda (x)
			     (list (car x)
				   (skk-jlod-aiueo-words-to-aoeui-list
				    (cadr x))))
			   '(("c" "$B$+$-$/$1$3(B")
			     ("s" "$B$5$7$9$;$=(B")
			     ("t" "$B$?$A$D$F$H(B")
			     ("n" "$B$J$K$L$M$N(B")
			     ("h" "$B$O$R$U$X$[(B")
			     ("m" "$B$^$_$`$a$b(B")
			     ("v" "$B$d$$$f$($h(B")
			     ("r" "$B$i$j$k$l$m(B")
			     ("g" "$B$,$.$0$2$4(B")
			     ("z" "$B$6$8$:$<$>(B")
			     ("d" "$B$@$B$E$G$I(B")
			     ("b" "$B$P$S$V$Y$\(B")
			     ("f" "$B$Q$T$W$Z$](B")))))))))

;; $BY92;3HD%(B
(defvar skk-jlod-youon-list
  (append
   ;; $BY92;3HD%(B $B$$9T(B
   (append-map (lambda (lis) (apply #'skk-jlod-make-rules lis))
	       (mapcar (lambda (x)
			 (list (car x)
			       skk-jlod-aoeui-list
			       (cadr x)
			       '("$B$c(B" "$B$g(B" "$B$'(B" "$B$e(B" "$B$#(B"
				 "$B$c$s(B" "$B$g$s(B" "$B$'$s(B" "$B$e$s(B" "$B$#$s(B"
				 "$B$c$$(B" "$B$g$&(B" "$B$'$$(B" "$B$e$&(B" "$B$e$$(B")))
		       '(("cg" "$B$-(B")
			 ("rg" "$B$j(B")
			 ("th" "$B$A(B")
			 ("nh" "$B$K(B")
			 ("sh" "$B$7(B")
			 ("zm" "$B$8(B")
			 ("fr" "$B$T(B")
			 ("gr" "$B$.(B")
			 ("dn" "$B$B(B")
			 ("hn" "$B$R(B")
			 ("bv" "$B$S(B")
			 ("mv" "$B$_(B"))))
   ;; $BY92;3HD%(B $B$&9T(B
   (append-map (lambda (lis) (apply #'skk-jlod-make-rules lis))
	       (mapcar (lambda (x)
			 (list (car x)
			       skk-jlod-aoeui-list
			       (cadr x)
			       '("$B$!(B" "$B$)(B" "$B$'(B" "$B$%(B" "$B$#(B"
				 "$B$!$s(B" "$B$)$s(B" "$B$'$s(B" "$B$%$s(B" "$B$#$s(B"
				 "$B$!$$(B" "$B$)$&(B" "$B$'$$(B" "$B$%$&(B" "$B$%$$(B")))
		       '(("hh" "$B$U(B")
			 ("wv" "$B$&(B")
			 ("vv" "$B$&!+(B"))))
   ;; $BY92;3HD%(B $B$(9T(B
   (append-map (lambda (lis) (apply #'skk-jlod-make-rules lis))
	       (mapcar (lambda (x)
			 (list (car x)
			       skk-jlod-aoeui-list
			       (cadr x)
			       '("$B$c(B" "$B$g(B" "$B$'(B" "$B$e(B" "$B$#(B"
				 "$B$c$s(B" "$B$g$s(B" "$B$'$s(B" "$B$e$s(B" "$B$#$s(B"
				 "$B$c$$(B" "$B$g$&(B" "$B$'$$(B" "$B$e$&(B" "$B$e$$(B")))
		       '(("dh" "$B$G(B")
			 ("tn" "$B$F(B"))))))

;; $BIQ=PY92;$N>JN,BG$A(B
(defvar skk-jlod-freq-youon-abbrev-list
  (append-map (lambda (lis) (apply #'skk-jlod-make-rules lis))
	      (append
	       ;; $B1&<j>eCJ(B
	       (mapcar (lambda (x) (list (car x) '("c" "l")
					 (cadr x) '("$B$e$&(B" "$B$g$&(B")))
		       '(("f" "$B$T(B")
			 ("g" "$B$.(B")
			 ("c" "$B$-(B")
			 ("r" "$B$j(B")))
	       ;; $B1&<jCfCJ(B
	       (mapcar (lambda (x) (list (car x) '("t" "s")
					 (cadr x) '("$B$e$&(B" "$B$g$&(B")))
		       '(("d" "$B$B(B")
			 ("h" "$B$R(B")
			 ("t" "$B$A(B")
			 ("n" "$B$K(B")
			 ("s" "$B$7(B")))
	       ;; $B1&<j2<CJ(B
	       (mapcar (lambda (x) (list (car x) '("w" "z")
					 (cadr x) '("$B$e$&(B" "$B$g$&(B")))
		       '(("b" "$B$S(B")
			 ("m" "$B$_(B")
			 ("z" "$B$8(B"))))))

;; $BY92;(B+$B!V$/!W!V$D!W$N>JN,BG$A(B
(defvar skk-jlod-youon-kutu-abbrev-list
  (append-map (lambda (lis) (apply #'skk-jlod-make-rules lis))
	      (append
	       ;; $B1&<j>eCJ(B
	       (mapcar (lambda (x)
			 (list (car x) '("g" "r" "l" "c")
			       (cadr x) '("$B$e$/(B" "$B$g$/(B" "$B$c$/(B" "$B$e$D(B")))
		       '(("fr" "$B$T(B")
			 ("gr" "$B$.(B")
			 ("cg" "$B$-(B")
			 ("rg" "$B$j(B")))
	       ;; $B1&<jCfCJ(B
	       (mapcar (lambda (x)
			 (list (car x) '("h" "n" "s" "t")
			       (cadr x) '("$B$e$/(B" "$B$g$/(B" "$B$c$/(B" "$B$e$D(B")))
		       '(("dn" "$B$B(B")
			 ("hn" "$B$R(B")
			 ("th" "$B$A(B")
			 ("nh" "$B$K(B")
			 ("sh" "$B$7(B")))
	       ;; $B1&<j2<CJ(B
	       (mapcar (lambda (x)
			 (list (car x) '("m" "v" "z" "w")
			       (cadr x) '("$B$e$/(B" "$B$g$/(B" "$B$c$/(B" "$B$e$D(B")))
		       '(("bv" "$B$S(B")
			 ("mv" "$B$_(B")
			 ("zm" "$B$8(B"))))))

;; $BJl2;(B+$B!V$-!W!V$/!W!V$D!W!V$C!W$N>JN,BG$A(B
(defvar skk-jlod-boin-kutu-abbrev-list
  (append-map (lambda (lis)
		(skk-jlod-make-rules
		 (car lis) skk-jlod-aoeui-list
		 (apply #'append
			(make-list 3 (skk-jlod-aiueo-words-to-aoeui-list
				      (cadr lis))))
		 (append-map (lambda (x) (make-list 5 x))
			     '("$B$D(B" "$B$C(B" "$B$/(B"))))
	      '( ;; $B1&<j>eCJ(B
		("ff" "$B$Q$T$W$Z$](B")
		("gf" "$B$,$.$0$2$4(B")
		("cf" "$B$+$-$/$1$3(B")
		("rf" "$B$i$j$k$l$m(B")
		;; $B1&<jCfCJ(B
		("dd" "$B$@$B$E$G$I(B")
		("hd" "$B$O$R$U$X$[(B")
		("td" "$B$?$A$D$F$H(B")
		("nd" "$B$J$K$L$M$N(B")
		("sd" "$B$5$7$9$;$=(B")
		;; $B1&<j2<CJ(B
		("bb" "$B$P$S$V$Y$\(B")
		("mb" "$B$^$_$`$a$b(B")
		("wb" "$B$o$p$U$q$r(B")
		("vb" "$B$d$$$f$($h(B")
		("zb" "$B$6$8$:$<$>(B"))))

;; $BIQ=P8l6g$N>JN,BG$A(B
(defvar skk-jlod-abbrev-words-list
  (append-map (lambda (lis) (skk-jlod-make-rules (car lis) "" (cadr lis) ""))
	      '(("ft" "$B$$$?$7$^$7$F(B")
		("fn" "$B$$$?$7$^$7$?(B")
		("fs" "$B$$$?$7$^$9(B")
		("gt" "$B$K$J$j$^$7$F(B")
		("gn" "$B$K$J$j$^$7$?(B")
		("gs" "$B$K$J$j$^$9(B")
		("cd" "$B$+$/$K$s(B")
		("ch" "$B$C$F(B")
		("ct" "$B$7$^$7$F(B")
		("cn" "$B$7$^$7$?(B")
		("cs" "$B$7$^$9(B")
		("cm" "$B$*$;$o$K(B")
		("rt" "$B$j$^$7$F(B")
		("rn" "$B$j$^$7$?(B")
		("rs" "$B$j$^$9(B")
		("dc" "$B$G$7$g$&$+(B")
		("tm" "$B$h$m$7$/(B")
		("tv" "$B$+$i(B")
		("nb" "$B$N$G(B")
		("sc" "$B$7$F(B")
		("sr" "$B$9$k(B")
		("sb" "$B$7$?(B")
		("sm" "$B$7$^(B")
		("bt" "$B$G$7$F(B")
		("bn" "$B$G$7$?(B")
		("bs" "$B$G$9(B")
		("mt" "$B$^$7$F(B")
		("mn" "$B$^$7$?(B")
		("ms" "$B$^$9(B")
		("wt" "$B$J$C$F$*$j$^$7$F(B")
		("wn" "$B$J$C$F$*$j$^$7$?(B")
		("ws" "$B$J$C$F$*$j$^$9(B")
		("zg" "$B$*$M$,$$(B")
		("zh" "$B$*$b$$(B"))))

(defvar skk-jlod-additional-rom-kana-rule-list
  (append skk-jlod-special-key-list skk-jlod-small-char-list skk-jlod-basic-list
	  skk-jlod-youon-list skk-jlod-freq-youon-abbrev-list
	  skk-jlod-youon-kutu-abbrev-list skk-jlod-boin-kutu-abbrev-list
	  skk-jlod-abbrev-words-list))


;; " : $B$O(B ' ; $B$H$7$FJQ49$5$;$k(B
(setq skk-downcase-alist
      (append skk-downcase-alist '((?\" . ?\') (?: . ?\;))))

;; '$B!V$C!W(B ;$B!V$"$s!W(B Q$B!V$*$s!W(B X$B!V$$$s!W(B $B$rJQ49%]%$%s%H$K2C$($k(B
(setq skk-set-henkan-point-key
      (append skk-set-henkan-point-key '(?\" ?: ?Q ?X)))

;; $B:GAa(B skk-rom-kana-base-rule-list $B$rCV$-49$($kJ}$,Aa$$$1$I(B
(let ((skk-jlod-unnecessary-rule-list
       (let ((prefix-list
	      (append-map (lambda (x) (list x (concat x x)))
			  '("j" "k" "p" "y"
			    "f" "g" "c" "r" "l"
			    "d" "h" "t" "n" "s"
			    "x" "b" "m" "w" "v" "z"
			    "ch" "dh" "th" "xk" "xw"))))
	 (append '("tsu" "xtsu" "xtu")
		 '("z-" "z/" "z[" "z]" "z{" "z}" "z(" "z)" "zl" "zL")
		 (mapcar #'car skk-jlod-additional-rom-kana-rule-list)
		 (append-map (lambda (y) (mapcar (lambda (x) (concat x y))
						 prefix-list))
			     '("" "a" "o" "e" "u" "i"
			       "ya" "yo" "ye" "yu" "yi"))))))
  ;; skk-rom-kana-base-rule-list $B$+$iJQ495,B'$r:o=|$9$k(B
  (dolist (str skk-jlod-unnecessary-rule-list)
    (setq skk-rom-kana-base-rule-list
	  (skk-del-alist str skk-rom-kana-base-rule-list)))
  ;; skk-rom-kana-rule-list $B$+$iJQ495,B'$r:o=|$9$k(B
  (dolist (str skk-jlod-unnecessary-rule-list)
    (setq skk-rom-kana-rule-list
	  (skk-del-alist str skk-rom-kana-rule-list)))
; skk-jlod-unnecessary-rule-list $B$,Bg$-$$$N$G<h$j4:$($:(B
; skk-jisx0201 $B$H$NJ;MQ$O%5%]!<%H$7$J$$(B
; $BA4$FCV$-49$($F$7$^$&$H$$$&$N$,$F$C$H$jAa$$$1$I(B
;  ;; for jisx0201
;  (eval-after-load "skk-jisx0201"
;    '(progn
;       (dolist (str skk-jlod-unnecessary-base-rule-list)
;	 (setq skk-jisx0201-base-rule-list
;	       (skk-del-alist str skk-jisx0201-base-rule-list)))
;
;       (let ((del-list '("hh" "mm")))
;	 (dolist (str del-list)
;	   (setq skk-jisx0201-base-rule-list
;		 (skk-del-alist str skk-jisx0201-base-rule-list))))
;
;       (dolist (rule skk-jlod-additional-rom-kana-rule-list)
;	 (add-to-list 'skk-jisx0201-rule-list
;		      (if (listp (nth 2 rule))
;			  (list (nth 0 rule) (nth 1 rule)
;				(japanese-hankaku (car (nth 2 rule))))
;			rule)))
;
;       (setq skk-jisx0201-base-rule-tree
;	     (skk-compile-rule-list skk-jisx0201-base-rule-list
;				    skk-jisx0201-rule-list))))
  )


;(defun skk-jlod-listup-confliction (word rule-list)
;  (defun cut-off (n lis)
;    (if (= n 0)
;	'()
;      (cons (car lis) (cut-off (- n 1) (cdr lis)))))
;  (defun cut (n lis)
;    (if (> n (length lis))
;	(list lis)
;      (cons (cut-off n lis) (cut n (nthcdr n lis)))))
;  (defun string-start-with-p (x y)
;    (defun aux (a b)
;      (cond ((null a)
;	     t)
;	    ((null b)
;	     nil)
;	    (t
;	     (and (= (car a) (car b))
;		  (aux (cdr a) (cdr b))))))
;    (aux (append x nil)
;	 (append y nil)))
;  (defun make-sub-list (word)
;    (defun aux (lis res)
;      (if (null lis)
;	  (mapcar (lambda (x) (apply #'concat x))
;		  (mapcar #'reverse (cdr res)))
;	(aux (cdr lis)
;	     (cons (cdr lis)
;		   res))))
;    (aux (mapcar #'string (reverse (append word nil))) '()))
;  (let ((sub-list (make-sub-list word)))
;    (append
;     (append-map (lambda (x) (srfi-1:filter (lambda (y) (string= x y))
;					    sub-list))
;		 rule-list)
;     (srfi-1:filter (lambda (x) (string-start-with-p word x))
;		    rule-list))))
;(defun my-fast-remove-dups (los)
;  (let ((hash (make-hash-table :test 'equal))
;        (ret nil))
;    (loop for s in los
;          do (puthash s nil hash))
;    (maphash (lambda (k v) (push k ret)) hash)
;    ret))
;
;(defvar skk-jlod-unnecessary-rule-list
;  (let ((word-list (append (mapcar #'car skk-jlod-additional-rom-kana-rule-list)
;			   '("b" "c" "d" "f" "g" "h" "j" "k" "m" "n"
;			     "p" "r" "s" "t" "v" "w" "x" "y" "z")))
;	(rule-list (append (srfi-1:filter #'stringp (mapcar #'car skk-rom-kana-base-rule-list))
;			   (mapcar #'car skk-rom-kana-rule-list))))
;; $B$I$&$;0l2s$7$+;H$o$J$$(B
;    (my-fast-remove-dups
;;    (remove-duplicates
;     (append-map (lambda (x)
;		   (skk-jlod-listup-confliction x rule-list))
;		 word-list)
;)
;;     :test 'equal)
;))
;
;;; skk-rom-kana-base-rule-list $B$+$iJQ495,B'$r:o=|$9$k(B
;(dolist (str skk-jlod-unnecessary-rule-list)
;  (setq skk-rom-kana-base-rule-list
;	(skk-del-alist str skk-rom-kana-base-rule-list)))
;;; skk-rom-kana-rule-list $B$+$iJQ495,B'$r:o=|$9$k(B
;(dolist (str skk-jlod-unnecessary-rule-list)
;  (setq skk-rom-kana-rule-list
;	(skk-del-alist str skk-rom-kana-rule-list)))
;
;;; for jisx0201
;(eval-after-load "skk-jisx0201"
;  '(progn
;     (dolist (str skk-jlod-unnecessary-base-rule-list)
;       (setq skk-jisx0201-base-rule-list
;	     (skk-del-alist str skk-jisx0201-base-rule-list)))
;
;     (let ((del-list '("hh" "mm")))
;       (dolist (str del-list)
;	 (setq skk-jisx0201-base-rule-list
;	       (skk-del-alist str skk-jisx0201-base-rule-list))))
;
;     (dolist (rule skk-jlod-additional-rom-kana-rule-list)
;       (add-to-list 'skk-jisx0201-rule-list
;		    (if (listp (nth 2 rule))
;			(list (nth 0 rule) (nth 1 rule)
;			      (japanese-hankaku (car (nth 2 rule))))
;		      rule)))
;
;     (setq skk-jisx0201-base-rule-tree
;	   (skk-compile-rule-list skk-jisx0201-base-rule-list
;				  skk-jisx0201-rule-list))))


;; JLOD $BFCM-$NJQ495,B'$rDI2C$9$k(B
(setq skk-rom-kana-rule-list
      (append skk-jlod-additional-rom-kana-rule-list
	      skk-rom-kana-rule-list))

(run-hooks 'skk-jlod-load-hook)

(provide 'skk-jlod)
;;; skk-jlod.el ends here
